angular.module('FinanceApp').config([
  '$stateProvider',
  '$urlRouterProvider',
  ($stateProvider, $urlRouterProvider) => {
    $stateProvider.state('dashboard', {
      url: '/dashboard',
      templateUrl: '/views/dashboard/home.html',
    })
    .state('billing_cycles', {
      url: '/billing_cycles?page',
      templateUrl: '/views/billing_cycles/index.html',
    })

    $urlRouterProvider.otherwise('/dashboard')
  }
])
