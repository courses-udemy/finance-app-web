(() => {
  angular.module('FinanceApp').component('paginator', {
    bindings: {
      url: '@',
      pages: '@'
    },
    controller: [
      '$location',
      function($location) {
        this.$onInit = () => {
          const pages = parseInt(this.pages) || 1

          this.arrayPages = Array(pages).fill(0).map((e, i) => i + 1)
          this.current = parseInt($location.search().page) || 1
          this.needPagination = this.pages > 1
          this.hasPrevious = this.current > 1
          this.hasNext = this.current < this.pages

          this.isCurrent = (i) => this.current == i
        }
      }
    ],
    template: `
    <div class="text-center">
      <ul ng-if="$ctrl.needPagination" class="pagination pagination-sm">
        <li ng-if="$ctrl.hasPrevious">
          <a href="{{ $ctrl.url }}?page={{ $ctrl.current - 1 }}">
            <i class="fa fa-angle-double-left"></i> Anterior
          </a>
        </li>

        <li ng-class="{active: $ctrl.isCurrent(index)}" ng-repeat="index in $ctrl.arrayPages">
          <a href="{{ $ctrl.url }}?page={{ index }}">{{ index }}</a>
        </li>

        <li ng-if="$ctrl.hasNext">
          <a href="{{ $ctrl.url }}?page={{ $ctrl.current + 1 }}">
            Próximo <i class="fa fa-angle-double-right"></i>
          </a>
        </li>
      </ul>
    </div>
    `
  })
})()
