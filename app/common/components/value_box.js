angular.module('FinanceApp').component('valueBox', {
  bindings: {
    grid: '@',
    color: '@',
    value: '@',
    text: '@',
    icon: '@'
  },
  controller: [
    'gridSystem',
    function(gridSystem) {
      this.$onInit = () => this.gridClasses = gridSystem.toCssClasses(this.grid)
    }
  ],
  template: `
  <div class="{{ $ctrl.gridClasses }}">
    <div class="small-box bg-{{ $ctrl.color }}">
      <div class="inner">
        <h3>R$ {{ $ctrl.value }}</h3>
        <p>{{ $ctrl.text }}</p>
      </div>
      <div class="icon">
        <i class="fa fa-{{ $ctrl.icon }}"></i>
      </div>
    </div>
  </div>
  `
})
