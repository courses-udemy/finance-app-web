(() => {
  angular.module('FinanceApp').factory('messages', [
    'toastr',
    function (toastr) {
      function addSuccess(messages) {
        addMsg(messages, 'Sucesso', 'success')
      }

      function addError(messages) {
        addMsg(messages, 'Erro', 'error')
      }

      function addMsg(messages, title, method) {
        if(messages instanceof Array) {
          messages.forEach(msg => toastr[method](msg, title))
        } else {
          toastr[method](messages, title)
        }
      }

      return { addSuccess, addError }
    }
  ])
})()
