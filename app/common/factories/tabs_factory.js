(() => {
  angular.module('FinanceApp').factory('tabs', [
    function (toastr) {
      function show(controller, {
        tabList = false,
        tabCreate = false,
        tabUpdate = false,
        tabDelete = false
      }) {
        controller.tabList = tabList
        controller.tabCreate = tabCreate
        controller.tabUpdate = tabUpdate
        controller.tabDelete = tabDelete
      }

      return { show }
    }
  ])
})()
