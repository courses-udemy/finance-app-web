(() => {
  angular.module('FinanceApp').controller('AuthController', [
    '$location',
    'messages',
    function($location, messages) {
      const vm = this
      vm.getUser = () => ({ name: 'Walmir Neto', email: 'wfsneto@gmail.com' })

      vm.logout = () => console.log('logout...')
    }
  ])
})()
