(() => {
  angular.module('FinanceApp').controller('DashboardController', [
    '$http',
    function($http) {
      const vm = this
      vm.summary = function () {
        const url = 'http://localhost:3003/api/billing_summary'

        $http.get(url).then((response) => {
          const { credit = 0, debt = 0 } = response.data
          vm.debt = debt
          vm.credit = credit
          vm.total = credit - debt
        })
      }

      vm.summary()
    }
  ])
})()
